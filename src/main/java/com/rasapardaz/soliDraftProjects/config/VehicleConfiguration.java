package com.rasapardaz.soliDraftProjects.config;

import com.rasapardaz.soliDraftProjects.services.PlainJdbcVehicleDao;
import com.rasapardaz.soliDraftProjects.services.VehicleDao;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class VehicleConfiguration {

    private static DataSource getDataSource(){
        HikariDataSource hikariDataSource=new HikariDataSource();
        hikariDataSource.setUsername("postgres");
        hikariDataSource.setPassword("1");
        hikariDataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/draftDB");
        hikariDataSource.setMinimumIdle(2);
        hikariDataSource.setMaximumPoolSize(5);
        return hikariDataSource;
    }

    public static VehicleDao getVehicleDao(){
        return new PlainJdbcVehicleDao(getDataSource());
    }
}
