package com.rasapardaz.soliDraftProjects;

import com.rasapardaz.soliDraftProjects.Entities.Vehicle;
import com.rasapardaz.soliDraftProjects.config.VehicleConfiguration;
import com.rasapardaz.soliDraftProjects.services.VehicleDao;

/**
 * Hello world!
 *
 */
public class App 
{


    public static void main( String[] args )
    {
        VehicleDao vehicleDao= VehicleConfiguration.getVehicleDao();
        Vehicle vehicle=new Vehicle("TT001","RED",4,4);
        vehicle=vehicleDao.findByVehicleNo(vehicle.getVehicleNo());
        if(vehicle==null)
        vehicleDao.insert(vehicle);
        else
            System.out.print("duplicate error for : "+vehicle);

    }
}
